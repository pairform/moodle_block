<?php
  $settings->add(new admin_setting_heading(
    'headerconfig', // Dans l'objet global $CFG, et stocké dans la base <prefix>_config (attention aux conflits)
    get_string('headerconfig', 'block_pairform'), //Texte principal
    get_string('descconfig', 'block_pairform') //Description
  ));

  //$allowHTML = get_config('simplehtml', 'Allow_HTML');
  $settings->add(new admin_setting_configcheckbox(
    'pairform/Allow_HTML', // Saved in <prefix>_config_plugin table, under your block's name. Your config data will still be available via a get_config() call.
    get_string('labelallowhtml', 'block_pairform'), //Texte principal
    get_string('descallowhtml', 'block_pairform'), //Description
    '0'
  ));