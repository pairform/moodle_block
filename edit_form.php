<?php
 
class block_pairform_edit_form extends block_edit_form {
 
    protected function specific_definition($mform) {
 
        // Section header title according to language file.
        $mform->addElement('header', 'config_header', get_string('blocksettings', 'block_pairform'));
 
        // A sample string variable with a default value.
        $mform->addElement('text', 'config_title', get_string('title', 'block_pairform'));
        $mform->setDefault('config_title', get_string('default_title', 'block_pairform'));
        $mform->setType('config_title', PARAM_TEXT);
 
        // A sample string variable with a default value.
        $mform->addElement('text', 'config_footer', get_string('footer', 'block_pairform'));
        $mform->setDefault('config_footer', '');
        $mform->setType('config_footer', PARAM_TEXT);        

        //Url de l'instance pairform
        $mform->addElement('text', 'config_url', get_string('url', 'block_pairform'));
        $mform->setDefault('config_url', get_string('default_url', 'block_pairform'));
        $mform->setType('config_url', PARAM_NOTAGS);

        //Secret
        $mform->addElement('text', 'config_module_name', get_string('module_name', 'block_pairform'));
        $mform->setDefault('config_module_name', get_string('default_module_name', 'block_pairform'));
        $mform->setType('config_module_name', PARAM_NOTAGS);
        //Secret
        $mform->addElement('text', 'config_secret', get_string('secret', 'block_pairform'));
        $mform->setDefault('config_secret', get_string('default_secret', 'block_pairform'));
        $mform->setType('config_secret', PARAM_NOTAGS);
    }
}