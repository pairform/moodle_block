<?php
$string['pluginname'] = 'Pairform';
$string['pairform'] = 'Pairform';
$string['pairform:addinstance'] = 'Ajouter un bloc Pairform';
$string['pairform:myaddinstance'] = 'Ajouter un bloc Pairform sur ma page personnelle';
$string['blocksettings'] = "Paramètrage du bloc";

$string['title'] = "Titre à afficher au dessus du module";
$string['footer'] = "Texte à afficher en dessous du module";
$string['secret'] = "Secret partagé";
$string['url'] = "Adresse de votre instance Pairform";
$string['module_name'] = "Nom du module (messages, community, succes…)";

$string['default_title'] = "Pairform";
$string['default_footer'] = "";
$string['default_url'] = "http://pairform.local/";
$string['default_secret'] = "pf_app_2018";
$string['default_module_name'] = "messages";