<?php
//Variables globales (https://docs.moodle.org/dev/Coding_style#Variables) : $CFG, $SESSION, $USER, $COURSE, $SITE, $PAGE, $DB, $THEME

//ATTENTION : Lancer composer avant d'installer via interface graphique.
require __DIR__.'/vendor/autoload.php';

use \Firebase\JWT\JWT;

class block_pairform extends block_base {
  //Attention : $this->config inaccessible
  public function init() {
    //Déclaration des valeurs par défaut
    //get_string va chercher dans lang/LANG/block_pairform.php le label
    $this->title = get_string('pairform', 'block_pairform');
    $this->footer = "";

    $this->shared_key = get_string('default_secret', 'block_pairform');
    
    $this->module_name = get_string('default_module_name', 'block_pairform');

    $this->url = get_string('default_url', 'block_pairform');
    $this->url_script = "$this->url/dist/modules/$this->module_name.js";
  }
  //Fonction principale
  public function get_content() {
    if ($this->content !== null) {
      return $this->content;
    }
    //Déclaration obligatoire des globales
    global $USER, $COURSE, $CFG, $SITE, $PAGE;

    //Obligation du login pour pouvoir accéder à $USER
    require_login();

    //Récupération de l'avatar si picture ≠ 0 (picture contient un int de version d'avatar pour invalider le cache apparemment)
    if ($USER->picture) {  
      //Voir https://moodle.org/mod/forum/discuss.php?d=319037#p1280570 pour référence
      //f1 : 100x100 / f2 : 50x50
      $avatar_url = $CFG->wwwroot.'/user/pix.php/'.$USER->id.'/f1.jpg'; 
    }

    //Création du token
    $token = array(
        "iss" => $CFG->wwwroot,
        // "iat" => time(), //Pas d'issued at, ça empecherait le cache de fonctionner

        "esp" => array(
          "uid" => $SITE->id,
          "fnm" => $SITE->fullname,
          "snm" => $SITE->shortname,
          "url" => $CFG->wwwroot
        ),
        "crs" => array(
          "uid" => $COURSE->id,
          "fnm" => $COURSE->fullname,
          "snm" => $COURSE->shortname
        ),
        "usr" => array(
          "uid" => $USER->id,
          "unm" => $USER->username,
          "eml" => $USER->email,
          "lng" => $USER->lang,
          "pic" => $avatar_url,
        )
    );

    //Si on est sur une activité
    if (preg_match("/^mod/", $PAGE->pagetype)) {
      //On ajoute ça au JWT
      $token["mod"] = array(
        "uid" => $PAGE->context->instanceid,
        "fnm" => $PAGE->title,
        "snm" => $PAGE->title,
        "typ" => $PAGE->pagetype
        // "url" => $PAGE->url->path // Impossible de récupérer path, il est déclaré comme protégé…
      );
    }

    /**
     * IMPORTANT:
     * You must specify supported algorithms for your application. See
     * https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40
     * for a list of spec-compliant algorithms.
     */
    $jwt = JWT::encode($token, $this->shared_key);


    //Génération du contenu
    $this->content = new stdClass;

    if (!empty($this->title)) {
      $this->content->title = $this->title;
    }

    //Inclusion de la balise qui sera bootstrapée par Angular, et dont le HTML sera chargée via la directive principale
    $this->content->text = "<pf-module $this->module_name ></pf-module>";
    //Chargement du bundle webpack (JS / CSS)
    $this->content->text .= "<script src='$this->url_script?ctx=$jwt' type='application/javascript'></script>";

    if (!empty($this->footer)) {
      $this->content->footer = $this->footer;
    }
    else {
      $this->content->footer   = '';
    }

    // Décommenter cette ligne pour tester des variables
    // $this->content->footer .= "<pre> token : ".print_r($token, true)."</pre>";

    return $this->content;
  }
  //Appelé immediatement après init()
  public function specialization() {
    if (isset($this->config)) {
      //Récupération des données renseignées sur cette instance
      $this->title = !empty($this->config->title) ? $this->config->title : get_string('default_title', 'block_pairform');
      $this->footer = !empty($this->config->footer) ? $this->config->footer : get_string('default_footer', 'block_pairform');
      $this->secret = !empty($this->config->secret) ? $this->config->secret : get_string('default_secret', 'block_pairform');
      $this->module_name = !empty($this->config->module_name) ? $this->config->module_name : get_string('default_module_name', 'block_pairform');
      
      $this->url = !empty($this->config->url) ? $this->config->url : get_string('default_url', 'block_pairform');
      $this->url_script = "$this->url/dist/modules/$this->module_name.js";
    }
  }
  //Permet de configurer plusieurs instances de block pairform
  public function instance_allow_multiple() {
    return true;
  }

  //Pour enclencher la configuration "globale" du block
  public function has_config() {
    return true;
  }
  public function hide_header() {
    return false;
  }

  //Ajout d'attributs sur le wrapper html du block
  public function html_attributes() {
    $attributes = parent::html_attributes(); // Récupérer les valeurs par défaut
    $attributes['class'] .= ' block_'. $this->name(); // On met le nom du block en class (= block_pairform)
    return $attributes;
  }
}

/* 
USER : stdClass Object
(
    [id] => 2
    [auth] => manual
    [confirmed] => 1
    [policyagreed] => 0
    [deleted] => 0
    [suspended] => 0
    [mnethostid] => 1
    [username] => admin
    [idnumber] => 
    [firstname] => Maen
    [lastname] => J
    [email] => mjuganaikloo@gmail.com
    [emailstop] => 0
    [icq] => 
    [skype] => 
    [yahoo] => 
    [aim] => 
    [msn] => 
    [phone1] => 
    [phone2] => 
    [institution] => 
    [department] => 
    [address] => 
    [city] => Nantes
    [country] => FR
    [lang] => en
    [calendartype] => gregorian
    [theme] => 
    [timezone] => 99
    [firstaccess] => 1529268732
    [lastaccess] => 1535375256
    [lastlogin] => 1535117496
    [currentlogin] => 1535362754
    [lastip] => 127.0.0.1
    [secret] => 
    [picture] => 0
    [url] => 
    [descriptionformat] => 1
    [mailformat] => 1
    [maildigest] => 0
    [maildisplay] => 1
    [autosubscribe] => 1
    [trackforums] => 0
    [timecreated] => 0
    [timemodified] => 1529268796
    [trustbitmask] => 0
    [imagealt] => 
    [lastnamephonetic] => 
    [firstnamephonetic] => 
    [middlename] => 
    [alternatename] => 
    [lastcourseaccess] => Array
        (
            [2] => 1533573906
            [3] => 1533574635
            [4] => 1535126818
        )

    [currentcourseaccess] => Array
        (
            [4] => 1535375256
        )

    [groupmember] => Array
        (
        )

    [profile] => Array
        (
        )

    [sesskey] => GqqQ7lwuRz
    [preference] => Array
        (
            [auth_manual_passwordupdatetime] => 1529268796
            [email_bounce_count] => 1
            [email_send_count] => 2
            [drawer-open-nav] => false
            [tool_usertours_tour_completion_time_1] => 1529268923
            [tool_usertours_tour_completion_time_2] => 1529269162
            [block_myoverview_last_tab] => courses
            [core_message_migrate_data] => 1
            [sidepre-open] => false
            [filepicker_recentrepository] => 4
            [filepicker_recentlicense] => allrightsreserved
            [filemanager_recentviewmode] => 1
            [login_failed_count_since_success] => 3
            [_lastloaded] => 1535375274
        )

    [ajax_updatable_user_prefs] => Array
        (
            [drawer-open-nav] => alpha
            [sidepre-open] => alpha
        )

    [access] => Array
        (
            [ra] => Array
                (
                    [/1] => Array
                        (
                            [7] => 7
                        )

                    [/1/2] => Array
                        (
                            [6] => 6
                        )

                )

            [time] => 1535362755
            [rsw] => Array
                (
                )

        )

    [enrol] => Array
        (
            [enrolled] => Array
                (
                )

            [tempguest] => Array
                (
                )

        )

    [editing] => 0
)

*/
/*
COURSE : stdClass Object
(
    [id] => 4
    [category] => 1
    [sortorder] => 10001
    [fullname] => HemoMOOC - session 2
    [shortname] => HemoMOOC - session 2
    [idnumber] => hemomooc
    [summary] => 


    [summaryformat] => 1
    [format] => weeks
    [showgrades] => 1
    [newsitems] => 3
    [startdate] => 1533592800
    [enddate] => 1536624000
    [marker] => 0
    [maxbytes] => 0
    [legacyfiles] => 0
    [showreports] => 0
    [visible] => 1
    [visibleold] => 1
    [groupmode] => 0
    [groupmodeforce] => 0
    [defaultgroupingid] => 0
    [lang] => fr
    [calendartype] => 
    [theme] => 
    [timecreated] => 1533571349
    [timemodified] => 1533573862
    [requested] => 0
    [enablecompletion] => 1
    [completionnotify] => 0
    [cacherev] => 1535044405
)
*/

/*
GLOBALS["site"]
stdClass Object
(
    [id] => 1
    [category] => 0
    [sortorder] => 1
    [fullname] => HemoMOOC
    [shortname] => HemoMOOC
    [idnumber] => 
    [summary] => HémoMOOC, plateforme d'information et de formation sur l'hémophilie mineure
    [summaryformat] => 0
    [format] => site
    [showgrades] => 1
    [newsitems] => 3
    [startdate] => 0
    [enddate] => 0
    [marker] => 0
    [maxbytes] => 0
    [legacyfiles] => 0
    [showreports] => 0
    [visible] => 1
    [visibleold] => 1
    [groupmode] => 0
    [groupmodeforce] => 0
    [defaultgroupingid] => 0
    [lang] => 
    [calendartype] => 
    [theme] => 
    [timecreated] => 1529268703
    [timemodified] => 1533573608
    [requested] => 0
    [enablecompletion] => 0
    [completionnotify] => 0
    [cacherev] => 1535384301
)
*/